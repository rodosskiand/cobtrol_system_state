#!/bin/sh

sleep 20
adduser --disabled-password --gecos "" celery
python3 manage.py migrate
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser \
		('admin', 'admin@myproject.com', 'iteketo')" | python3 manage.py shell
service supervisor start
exec "$@"


