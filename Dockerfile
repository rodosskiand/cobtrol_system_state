FROM ubuntu


RUN apt-get update && \
  apt-get install -y apt-utils nano curl apache2 apache2-utils default-libmysqlclient-dev python3 libapache2-mod-wsgi-py3 python3-pip nmap supervisor 

COPY ./   /app/
RUN pip3 install --upgrade pip
RUN pip3 install -r /app/requirements.txt
ADD ./000-default.conf /etc/apache2/sites-available/000-default.conf
ADD ./.htpasswd /etc/apache2/.htpasswd
ADD ./celery.conf /etc/supervisor/conf.d/celery.conf
CMD ["apache2ctl", "-D", "FOREGROUND"]
ENTRYPOINT [ "/bin/bash", "./migrate.sh"]
