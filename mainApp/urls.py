from django.urls import path, include
from . import views
from django.conf.urls import url
from mainApp.models import Device
from mainApp.models import Inf_sys
from django.views.generic import ListView, DetailView




urlpatterns = [
 	url(r'^$',views.index_home, name='index_home'),
    url(r'^systems$',views.index, name='index'),
    path('inf_sys/<str:pk>/',views.index, name='index'),
    path('inf_sys/<str:pk>/ssh_create',views.get_ssh1, name='get_ssh'),
    path('inf_sys/<str:pk>/ssh_compare',views.compare_ssh, name='compare_ssh'),
    path('inf_sys/<str:pk>/device',views.device, name='device'),
    path('<int:pk>/delete',views.delete_device, name='delete_device'),
    path('add_inf_sys',views.Add_inf_Sys, name='Add_inf_Sys'),
    path('<int:pk>/edit',views.edit_device, name='edit_device'),
    path('inf_sys/<str:pk>/edit_inf_sys',views.edit_inf_Sys, name='edit_inf_Sys'),
    path('<int:pk>/ssh_create_device/',views.get_ssh_device, name='get_ssh_device'),
    path('<int:pk>/ssh_compare_device/',views.ssh_compare_device, name='ssh_compare_device'),
    path('<int:pk>/',views.device_detail, name='device_detail'),
]



