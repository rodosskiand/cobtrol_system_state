# Generated by Django 2.0 on 2019-05-07 11:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0015_hash_service_conf_changed_text_conf'),
    ]

    operations = [
        migrations.CreateModel(
            name='State_of_task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('task_state', models.CharField(max_length=64)),
            ],
        ),
    ]
