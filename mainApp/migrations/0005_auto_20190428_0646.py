# Generated by Django 2.0 on 2019-04-28 06:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0004_auto_20190428_0643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='hash_service_conf',
            name='text_conf',
            field=models.TextField(blank=True),
        ),
    ]
