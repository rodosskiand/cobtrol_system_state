# Generated by Django 2.0 on 2019-04-28 08:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0005_auto_20190428_0646'),
    ]

    operations = [
        migrations.CreateModel(
            name='Win_program',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('path', models.CharField(max_length=64, null=True)),
                ('device', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mainApp.Device')),
            ],
        ),
        migrations.AddField(
            model_name='hash_service_conf',
            name='name_windows',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='mainApp.Win_program'),
        ),
    ]
