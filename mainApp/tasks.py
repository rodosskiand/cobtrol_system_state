# -*- coding: utf-8 -*-
from celery.task import task
from . import views
from django.db.models import Q
from celery.decorators import periodic_task
from docx import Document
from docx.shared import Inches
from io import BytesIO
from pypsexec.client import Client


from mainApp.models import Device
from mainApp.models import Service
from mainApp.models import Reference_port
from mainApp.models import Hash_service_conf
from mainApp.models import Inf_sys
from mainApp.models import Report_scan
from mainApp.models import Report_compare
from mainApp.models import State_of_task
from mainApp.models import Report_scan_device
from mainApp.models import Report_compare_device
from mainApp.models import Win_program

import paramiko 
import re 
import nmap
import datetime
import traceback


executable = "cmd.exe"


@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def compare(pk): # сравнение текущего состояния с эталонным
	State_of_task.objects.all().delete() #очищаем таблицу состояний сканирований
	p = State_of_task(task_compare_state="Идет сканирование...")#записываем текущее состояние сканирования
	p.save()
	try:
		#создаем экземпляр объекта SSHClient для подключения по SSH
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

		now = datetime.datetime.now()#текущая дата и время (необходимо для метки)
		#report = ''
		devices = Device.objects.filter(inf_sys=pk) #получаем список устройств из БД
		inf_system = Inf_sys.objects.filter(name=pk)[0]
		document = Document()
		for device in devices:
			if device.check == False: #Если устройсто требуется проверять, то проверяем 
				continue
			else:

				try:
					# подключаемся по ssh, используя данные из БД с паролем
					if device.smb == True:
						c = Client(device.ip, username=device.user, password=device.password)
						c.connect()
						c.create_service()
					else:	
						client.connect(hostname=device.ip, username=device.user, password=device.password, port=device.port) 
				except Exception as e:
					try:
						# подключаемся по ssh, используя данные из БД с RSA
						key = StringIO('''-----BEGIN RSA PRIVATE KEY-----\n'''+device.key+
							'''\n
							-----END RSA PRIVATE KEY-----\n''')
						privkey = paramiko.RSAKey.from_private_key(key)
						client.connect(hostname=device.ip, username=device.user, port=device.port,pkey=privkey)
					except:
						continue

				#получаем имя устройства
				if device.smb == True:
					name_host = c.run_executable(executable, arguments="/c hostname")
					hostname= name_host[0].decode('cp866')
				else:
					stdin_hostname, stdout_hostname, stderr_hostname = client.exec_command('hostname')
					hostname = str(stdout_hostname.read().decode('cp866'))
				#заголовок документа с именем устройства
				document.add_heading(hostname, 0)

				#определяем открытые порты на устройстве
				nmap_result = views.scan_ports(device.ip, inf_system.args)
				#пишем все в документ
				document.add_heading("Changes in ports:", 0)

				#список открытых портов (в БД) 
				ports_in_base = Reference_port.objects.filter(device_of_port=device,inf_sys=inf_system)

				#процедура сравнения портов
				old_ports = []
				result_compare =''
				for res in nmap_result:
					if len(res)<1:
						continue
					res = re.sub('[^0-9]+', '', res)
					old_ports.append(res)
					for port in ports_in_base:
						if str(port.number) == res:
							break
					else:
						result_compare = result_compare + '\nПорт ' + res + ' открыт (был закрыт),' 
					
				for port in ports_in_base:
					try:
						old_ports.index(str(port.number))
					except Exception as e:
						result_compare = result_compare + '\nПорт ' +str(port.number) + ' закрыт (был открыт)' 
				document.add_paragraph(result_compare)
				

				#определяем семейство ОС (windows/linux)
				win = -1
				if device.smb == True:
					ver_os = c.run_executable(executable, arguments="/c ver")
					win = ver_os[0].decode('cp866').lower().find('windows')
				else:
					stdin, stdout, stderr = client.exec_command('ver')
					try:
						win = stdout.read().decode('cp866').split('\n')[1].lower().find('windows')
					except:
						pass
				if win != -1:
					print('win')
					if device.smb == True:
						views.ssh_compare_windows(c, document, device, inf_system,now)
						c.remove_service()
						c.disconnect()
					else:
						views.ssh_compare_windows(client, document, device, inf_system,now)
						client.close()
					document.add_page_break()
				else:	
					print('lin')
					views.ssh_compare_linux(client, document, device, inf_system,now)
					document.add_page_break()
					client.close()
		report = ""

		#запимываем отчет в БД и сохраняем отчет на диск
		for para in document.paragraphs:
			report = report +para.text+'\n'
		if Report_compare.objects.filter(inf_sys=inf_system).exists() == False:
			Report_compare(inf_sys=inf_system, description=report).save()
		else:		
			Report_compare_obj = Report_compare.objects.filter(inf_sys=inf_system).last()		
			Report_compare_obj.description = report
			Report_compare_obj.inf_sys = inf_system 
			Report_compare_obj.save()

		document.save('/app/reports/'+'compare:'+str(now)+'.docx')
		p = State_of_task(task_compare_state="Сканирование завершилось успешно")
		p.save()
	# except:
	# 	p = State_of_task(task_compare_state="Сканирование завершилось неудачно")
	# 	p.save()
	except Exception as e:
		p = State_of_task(task_compare_state="Сканирование завершилось неудачно")
		p.save()
		print('Ошибка:\n', traceback.format_exc())



    

@task( max_retries=1, default_retry_delay=10)
def scan(pk): # подключение по ssh к хосту и выполнение сканирования nmap(создание)
	State_of_task.objects.all().delete()
	p = State_of_task(task_state="Идет сканирование...")
	p.save()
	try:
		#создаем экземпляр объекта SSHClient для подключения по SSH
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		#очищаем все данные в бд
		Reference_port.objects.filter(inf_sys=pk).delete()
		Hash_service_conf.objects.filter(inf_sys=pk).delete()
		Service.objects.filter(inf_sys=pk).delete()
		Win_program.objects.all().delete()
		#получаем список устройств из БД
		devices = Device.objects.filter(inf_sys=pk)
		inf_system = Inf_sys.objects.filter(name=pk)[0]
		document = Document()
		# временная метка (нужна)
		now = datetime.datetime.now()
		for device in devices:
			if device.check == False: #Если устройсто требуется проверять, то проверяем 
				continue
			else:

				try:
					# подключаемся по ssh, используя данные из БД с паролем
					if device.smb == True:
						c = Client(device.ip, username=device.user, password=device.password)
						c.connect()
						c.create_service()
					else:	
						client.connect(hostname=device.ip, username=device.user, password=device.password, port=device.port) # подключаемся по ssh, используя данные из БД
				except Exception as e:
					try:
						# подключаемся по ssh, используя данные из БД с RSA
						key = StringIO('''-----BEGIN RSA PRIVATE KEY-----\n'''+device.key+
							'''\n
							-----END RSA PRIVATE KEY-----\n''')
						privkey = paramiko.RSAKey.from_private_key(key)
						client.connect(hostname=device.ip, username=device.user,  port=device.port,pkey=privkey)
					except:
						continue

				#получаем имя устройства
				if device.smb == True:
					name_host = c.run_executable(executable, arguments="/c hostname")
					hostname= name_host[0].decode('cp866')
				else:
					stdin_hostname, stdout_hostname, stderr_hostname = client.exec_command('hostname')
					hostname = str(stdout_hostname.read().decode('cp866'))
				#заголовок документа с именем устройства
				document.add_heading(hostname, 0)

				#определяем открытые проты
				nmap_result = views.scan_ports(device.ip, inf_system.args)
		
				#запись эталона для портов в БД
				for res in nmap_result:
					#res = re.sub('[^0-9]+', '', res)
					if not Reference_port.objects.filter(number=res, device_of_port=device,inf_sys=inf_system ).exists() :
						rp = Reference_port(number=res,
					        device_of_port=device,
					        inf_sys=inf_system)
						rp.save()

				#проверка ОС и выполнение функции сканирования
				win = -1
				if device.smb == True:
					ver_os = c.run_executable(executable, arguments="/c ver")
					win = ver_os[0].decode('cp866').lower().find('windows')
					winver = ver_os[0].decode('cp866')
				else:
					stdin, stdout, stderr = client.exec_command('ver')
					try:
						winver = stdout.read().decode('cp866')
						win = winver.split('\n')[1].lower().find('windows')
					except:
						pass
				if win != -1:
					#пишем в документ открытые порты и версию ОС
					device.os = winver
					document.add_paragraph(winver) 
					document.add_heading("Open ports:", 0)
					document.add_paragraph(','.join(nmap_result)) 
					#запускаем сканирование
					if device.smb == True:
						views.ssh_scan_windows(c, document, device, inf_system)
						c.remove_service()
						c.disconnect()
					else:
						views.ssh_scan_windows(client, document, device, inf_system)
						client.close()
					device.save()
					document.add_page_break()
				else:
					#пишем в документ открытые порты и версию ОС
					stdin, stdout, stderr = client.exec_command('uname -srvo')
					device.os = stdout.read().decode('cp866')
					document.add_paragraph(device.os) 
					document.add_heading("Open ports:", 0)
					document.add_paragraph(','.join(nmap_result)) 
					device.save()
					#запускаем сканирование
					views.ssh_scan_linux(client, document, device, inf_system)
					document.add_page_break()
					client.close()

		#Записываем результаты
		report = ""
		for para in document.paragraphs:
			report = report +para.text+'\n'
		document.save('/app/reports/scan'+str(now)+'.docx')

		if  Report_scan.objects.filter(inf_sys=inf_system).exists() == False:
			Report_scan_obj = Report_scan(description=report, inf_sys=inf_system).save()
		else:	
			Report_scan_obj = Report_scan.objects.filter(inf_sys=inf_system).last()
			Report_scan_obj.description = report
			Report_scan_obj.inf_sys = inf_system 
			Report_scan_obj.save()
		p = State_of_task(task_state="Сканирование завершилось успешно")
		p.save()
	# except:
	# 	p = State_of_task(task_state="Сканирование завершилось неудачно")
	# 	p.save()
	except Exception as e:
		p = State_of_task(task_state="Сканирование завершилось неудачно")
		p.save()
		print('Ошибка:\n', traceback.format_exc())



@task(ignore_result=True, max_retries=1, default_retry_delay=10)
def compare_device(pk): # сравнение состоянией (для устройства)
	State_of_task.objects.all().delete()
	p = State_of_task(task_compare_device_state="Идет сканирование...")
	p.save()
	try:
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

		now = datetime.datetime.now()
		report = ''
		document = Document()
		device = Device.objects.filter(id=pk)[0]
		if device.check == False: #Если устройсто требуется проверять, то проверяем 
			exit()
		else:

			try:
				if device.smb == True:
						c = Client(device.ip, username=device.user, password=device.password)
						c.connect()
						c.create_service()
				else:	
					client.connect(hostname=device.ip, username=device.user, password=device.password, port=device.port) # подключаемся по ssh, используя данные из БД
			except Exception as e:
				try:
					key = StringIO('''-----BEGIN RSA PRIVATE KEY-----\n'''+device.key+
						'''\n
						-----END RSA PRIVATE KEY-----\n''')
					privkey = paramiko.RSAKey.from_private_key(key)
					client.connect(hostname=device.ip, username=device.user, port=port,pkey=privkey)
				except:
					exit()
			#получаем имя устройства
			if device.smb == True:
				name_host = c.run_executable(executable, arguments="/c hostname")
				hostname= name_host[0].decode('cp866')
			else:
				stdin_hostname, stdout_hostname, stderr_hostname = client.exec_command('hostname')
				hostname = str(stdout_hostname.read().decode('cp866'))
			#заголовок документа с именем устройства
			document.add_heading(hostname, 0)
			nmap_result = views.scan_ports(device.ip, device.args)
			# #пишем все в документ
			document.add_heading("Changes in ports:", 0)
			# #document.add_paragraph(nmap_result) 
			ports_in_base = Reference_port.objects.filter(device_of_port=device,inf_sys=device.inf_sys)
			old_ports = []
			result_compare =''
			for res in nmap_result:
				if len(res)<1:
					continue
				res = re.sub('[^0-9]+', '', res)
				old_ports.append(res)
				for port in ports_in_base:
					if str(port.number) == res:
						break
				else:
					result_compare = result_compare + '\nПорт ' + res + ' открыт (был закрыт),' 
				
			for port in ports_in_base:
				try:
					old_ports.index(str(port.number))
				except Exception as e:
					result_compare = result_compare + '\nПорт ' +str(port.number) + ' закрыт (был открыт)' 
			document.add_paragraph(result_compare)

				
			win = -1
			if device.smb == True:
				ver_os = c.run_executable(executable, arguments="/c ver")
				win = ver_os[0].decode('cp866').lower().find('windows')
			else:
				stdin, stdout, stderr = client.exec_command('ver')
				try:
					win = stdout.read().decode('cp866').split('\n')[1].lower().find('windows')
				except:
					pass
			if win != -1:
				if device.smb == True:
					views.ssh_compare_windows(c, document, device, device.inf_sys, now)
					c.remove_service()
					c.disconnect()
				else:
					views.ssh_compare_windows(client, document, device, device.inf_sys, now)
					client.close()
				document.add_page_break()
			else:	
				views.ssh_compare_linux(client, document, device, device.inf_sys, now)
				document.add_page_break()
				client.close()

		report = ""
		for para in document.paragraphs:
			report = report +para.text+'\n'

		if  Report_compare_device.objects.filter(device=device).exists() == False:
			Report_scan_obj = Report_compare_device(description=report, inf_sys=device.inf_sys, device=device).save()
		else:		
			Report_scan_obj = Report_compare_device.objects.filter(device=device).last()
			Report_scan_obj.description = report
			Report_scan_obj.device = device
			Report_scan_obj.inf_sys = device.inf_sys 
			Report_scan_obj.save()
		document.save('/app/reports/compare-'+str(hostname)+str(now)+'.docx')
		p = State_of_task(task_compare_device_state="Сканирование завершилось успешно")
		p.save()
	# except:
	# 	p = State_of_task(task_compare_device_state="Сканирование завершилось неудачно")
	# 	p.save()
	except Exception as e:
		print('Ошибка:\n', traceback.format_exc())


@task( max_retries=1, default_retry_delay=10)
def scan_device(pk): # создание эталона (для устройства)
	State_of_task.objects.all().delete()
	p = State_of_task(task_scan_device_state="Идет сканирование...")
	p.save()
	try:
		client = paramiko.SSHClient()
		client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		device = Device.objects.filter(id=pk)[0]
		Reference_port.objects.filter(device_of_port=pk).delete()
		Hash_service_conf.objects.filter(device_of_hash=pk).delete()
		Service.objects.filter(device=pk).delete()
		Win_program.objects.filter(device=pk).delete()
		document = Document()
		now = datetime.datetime.now()
		if device.check == False: #Если устройсто требуется проверять, то проверяем 
			exit()
		else:

			try:
				if device.smb == True:
					c = Client(device.ip, username=device.user, password=device.password)
					c.connect()
					c.create_service()
				else:	
					client.connect(hostname=device.ip, username=device.user, password=device.password, port=device.port) # подключаемся по ssh, используя данные из БД
			except Exception as e:
				try:
					key = StringIO('''-----BEGIN RSA PRIVATE KEY-----\n'''+device.key+
						'''\n
						-----END RSA PRIVATE KEY-----\n''')
					privkey = paramiko.RSAKey.from_private_key(key)
					client.connect(hostname=device.ip, username=device.user, port=device.port,pkey=privkey)
				except:
					exit()

			#получаем имя устройства
			if device.smb == True:
				name_host = c.run_executable(executable, arguments="/c hostname")
				hostname= name_host[0].decode('cp866')
			else:
				stdin_hostname, stdout_hostname, stderr_hostname = client.exec_command('hostname')
				hostname = str(stdout_hostname.read().decode('cp866'))
			#заголовок документа с именем устройства
			document.add_heading(hostname, 0)
			nmap_result = views.scan_ports(device.ip, device.args)

			#запись эталона для портов
			for res in nmap_result:
				#res = re.sub('[^0-9]+', '', res)
				if not Reference_port.objects.filter(number=res, device_of_port=device,inf_sys=device.inf_sys ).exists() :
					rp = Reference_port(number=res,
				        device_of_port=device,
				        inf_sys=device.inf_sys)
					rp.save()

			#проверка ОС и выполнение функции сканирования
			win = -1
			if device.smb == True:
				ver_os = c.run_executable(executable, arguments="/c ver")
				win = ver_os[0].decode('cp866').lower().find('windows')
				winver = ver_os[0].decode('cp866')
			else:
				stdin, stdout, stderr = client.exec_command('ver')
				try:
					winver = stdout.read().decode('cp866')
					win = winver.split('\n')[1].lower().find('windows')
				except:
					pass
			if win != -1:
				device.os = winver
				document.add_paragraph(winver) 
				#пишем все в документ
				document.add_heading("Open ports:", 0)
				document.add_paragraph(','.join(nmap_result)) 
				device.save()
				if device.smb == True:
					views.ssh_scan_windows(c, document, device, device.inf_sys)
					c.remove_service()
					c.disconnect()
				else:
					views.ssh_scan_windows(client, document, device, device.inf_sys)
					client.close()
				document.add_page_break()
			else:
				stdin, stdout, stderr = client.exec_command('uname -srvo')
				device.os = stdout.read().decode('cp866')
				document.add_paragraph(device.os) 
				#пишем все в документ
				document.add_heading("Open ports:", 0)
				document.add_paragraph(','.join(nmap_result)) 
				views.ssh_scan_linux(client, document, device, device.inf_sys)
				device.save()
				document.add_page_break()
				client.close()

		report = ""
		for para in document.paragraphs:
			report = report +para.text+'\n'

		document.save('/app/reports/scan-'+str(hostname)+str(now)+'.docx')
		if  Report_scan_device.objects.filter(device=device).exists() == False:
			Report_scan_obj = Report_scan_device(description=report, inf_sys=device.inf_sys, device=device)
			Report_scan_obj.save()
		else:
			Report_scan_obj = Report_scan_device.objects.filter(device=device).last()
			Report_scan_obj.description = report
			Report_scan_obj.device = device
			Report_scan_obj.inf_sys = device.inf_sys 
			Report_scan_obj.save()
		p = State_of_task(task_scan_device_state="Сканирование завершилось успешно")
		p.save()
	# except:
	# 	p = State_of_task(task_scan_device_state="Сканирование завершилось неудачно")
	# 	p.save()
	except Exception as e:
		p = State_of_task(task_scan_device_state="Сканирование завершилось неудачно")
		p.save()
		print('Ошибка:\n', traceback.format_exc())