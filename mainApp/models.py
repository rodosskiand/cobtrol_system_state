from django.db import models
from django_cryptography.fields import encrypt


class Inf_sys(models.Model):#информационные системы
	name = models.CharField(max_length=64, primary_key=True)
	owner = models.CharField(max_length=64)
	description = models.CharField(max_length=256)
	args = models.CharField(max_length=64, null=True)

class Device(models.Model):#устройства
	name = models.CharField(max_length=64)
	ip = models.CharField(max_length=64)
	main_gate = models.CharField(max_length=64)
	user = models.CharField(max_length=64)
	password = models.CharField(max_length=64)
	key = models.CharField(max_length=2048, null=True)
	smb = models.BooleanField(default=False)
	nmap = models.BooleanField()
	typeof = models.CharField(max_length=64)
	check = models.BooleanField(default=False)
	inf_sys = models.ForeignKey(Inf_sys, on_delete=models.CASCADE,null=True)
	port = models.IntegerField(default=22)
	os = models.CharField(max_length=512, null=True)
	args = models.CharField(max_length=64, null=True)



class Service(models.Model):#сервисы linux
	name = models.CharField(max_length=64)
	device = models.ForeignKey(Device, on_delete=models.CASCADE,null=True)
	path = models.CharField(max_length=512)
	inf_sys = models.ForeignKey(Inf_sys, on_delete=models.CASCADE,null=True)
	version = models.CharField(max_length=256, null=True) 


class Win_program(models.Model):#сервисы windows
	name = models.CharField(max_length=64)
	device = models.ForeignKey(Device, on_delete=models.CASCADE)
	path = models.CharField(max_length=512, null=True)
	version = models.CharField(max_length=128, null=True) 



class Reference_port(models.Model):#открытые порты
	number = models.IntegerField()
	device_of_port = models.ForeignKey(Device, on_delete=models.CASCADE)
	inf_sys = models.ForeignKey(Inf_sys, on_delete=models.CASCADE,null=True)


class Hash_service_conf(models.Model):#хэш конф файлов сервисов
	name = models.CharField(max_length=64)
	name_windows = models.ForeignKey(Win_program, on_delete=models.CASCADE,null=True,related_name='windent')
	name_linux = models.ForeignKey(Service, on_delete=models.CASCADE,null=True,related_name='lindent')
	file = models.CharField(max_length=1024,default='SOME STRING')
	hash_conf = models.CharField(max_length=256)
	device_of_hash = models.ForeignKey(Device, on_delete=models.CASCADE)
	inf_sys = models.ForeignKey(Inf_sys, on_delete=models.CASCADE,null=True)
	text_conf = encrypt(models.TextField(null=True, blank=True))
	changed_text_conf = encrypt(models.TextField(null=True, blank=True))
	timestamp = models.CharField(max_length=64, null = True) 
	#states
	changed = models.BooleanField(default=False)
	deleted = models.BooleanField(default=False)
	new = models.BooleanField(default=False)





class Report_scan(models.Model):#report scan
	description = models.TextField()
	inf_sys = models.ForeignKey(Inf_sys, on_delete=models.CASCADE,null=True)
	data= models.DateTimeField(auto_now=True)


class Report_compare(models.Model):#report compare
	description = models.TextField()
	inf_sys = models.ForeignKey(Inf_sys, on_delete=models.CASCADE,null=True)
	data= models.DateTimeField(auto_now=True)

class Report_scan_device(models.Model):#report scan
	description = models.TextField()
	device = models.ForeignKey(Device, on_delete=models.CASCADE)
	inf_sys = models.ForeignKey(Inf_sys, on_delete=models.CASCADE,null=True)
	data= models.DateTimeField(auto_now=True)

class Report_compare_device(models.Model):#report scan
	description = models.TextField()
	device = models.ForeignKey(Device, on_delete=models.CASCADE)
	inf_sys = models.ForeignKey(Inf_sys, on_delete=models.CASCADE,null=True)
	data= models.DateTimeField(auto_now=True)


class State_of_task(models.Model):
	task_state = models.CharField(max_length=64,null=True)
	task_compare_state = models.CharField(max_length=64,null=True)
	task_scan_device_state = models.CharField(max_length=64,null=True)
	task_compare_device_state = models.CharField(max_length=64,null=True)











