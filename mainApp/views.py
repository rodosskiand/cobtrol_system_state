from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from docx import Document
from pypsexec.client import Client
from io import BytesIO
from django.db.models import Q
from . import tasks
from difflib import *

from mainApp.models import Device
from mainApp.models import Service
from mainApp.models import Reference_port
from mainApp.models import Hash_service_conf
from mainApp.models import Inf_sys
from mainApp.models import Report_scan
from mainApp.models import Report_compare
from mainApp.models import Win_program
from mainApp.models import State_of_task
from mainApp.models import Report_scan_device
from mainApp.models import Report_compare_device

import paramiko 
import re 
import nmap
import datetime

executable = "cmd.exe"

#функция для сканирования портов
def scan_ports(ip,args):
	nm = nmap.PortScanner()
	nm.scan(hosts=ip, arguments=args)
	#print(nm.scan(hosts=ip, arguments=args))
	ports = nm[ip]['tcp'].keys()
	new_ports = []
	for port in ports:
		new_ports.append(str(port))
	return new_ports

#добавление информационной системы
def Add_inf_Sys(request):#add inf sys id db
	name_sys = request.GET['name_sys']
	user = request.GET['user']
	description =	request.GET['description'] 
	args = request.GET['args']
	p = Inf_sys(name=name_sys,
    	owner=user,
        description=description,
        args=args)
	p.save()
	return HttpResponseRedirect('/')

#изменение информационной системы
def edit_inf_Sys(request,pk):
	p = Inf_sys.objects.filter(name=pk)[0]
	path = '/inf_sys/'+str(p.name)+'/'
	name = request.GET['name_sys']
	owner = request.GET['user']
	description =	request.GET['description'] 
	args = request.GET['args']
	p.name=name
	p.owner=owner
	p.description=description
	p.args=args
	p.save()
	return HttpResponseRedirect('/inf_sys/'+str(pk)+'/')

#добавляем устройство в БД
def device(request,pk):
	nameclient = request.GET['nameclient']
	ipclient = request.GET['ipclient']
	maingate =	request.GET['maingate'] 
	user = request.GET['user']
	password = request.GET['password']
	smb = request.GET.get('smb', False)
	nmap = request.GET.get('nmap',False)
	typeof = request.GET.get('typeof','Компьютер')
	chaeck = request.GET.get('check',False)
	port = request.GET.get('port',22)
	key = request.GET.get('sshkey',22)
	args = request.GET.get('args')
	if port == '':
		port = 22
	p = Device(name=nameclient,
    	ip=ipclient,
        main_gate=maingate,
      	user=user,
        password=password,
        smb=smb,
        key=key,
        nmap=nmap,
        typeof=typeof,
        check=chaeck,
        inf_sys=Inf_sys.objects.filter(name=pk)[0],
        port=port,
        args=args)
	p.save()
	data = {"ip": get_client_ip(request) ,'namehost':get_client_name(request), 'remotehost':get_host(request), 
		'devices': Device.objects.filter(inf_sys=pk), 'pk':pk}
	path = '/inf_sys/'+str(p.inf_sys.name)+'/'
	return HttpResponseRedirect(path)

#удаляем устройство из БД
def delete_device(request,pk):
	p = Device.objects.filter(id=pk)[0]
	path = '/inf_sys/'+str(p.inf_sys.name)+'/'
	data = {"ip": get_client_ip(request) ,'namehost':get_client_name(request), 'remotehost':get_host(request), 
		'devices': Device.objects.filter(inf_sys=p.inf_sys), 'pk':p.inf_sys}
	p.delete()
	#return render(request, 'mainApp/wrapper.html',context=data)
	return HttpResponseRedirect(path)

#редактирование устройства
def edit_device(request,pk):
	p = Device.objects.filter(id=pk)[0]
	path = '/inf_sys/'+str(p.inf_sys.name)+'/'
	nameclient = request.GET['nameclient']
	ipclient = request.GET['ipclient']
	maingate =	request.GET['maingate'] 
	user = request.GET['user']
	password = request.GET['password']
	smb = request.GET.get('smb', False)
	sshkey = request.GET['sshkey']
	nmap = request.GET.get('nmap',False)
	typeof = request.GET.get('typeof','Компьютер')
	chaeck = request.GET.get('check',False)
	port = request.GET['port']
	args = request.GET.get('args')
	if port == '':
		port = 22
	p.name=nameclient
	p.ip=ipclient
	p.main_gate=maingate
	p.user=user
	p.password=password
	p.smb = smb
	p.key = sshkey
	p.nmap=nmap
	p.typeof=typeof
	p.check=chaeck
	p.port=port
	p.inf_sys=p.inf_sys
	p.args = args
	p.save()
	data = {"ip": get_client_ip(request) ,'namehost':get_client_name(request), 'remotehost':get_host(request), 
		'devices': Device.objects.filter(inf_sys=p.inf_sys), 'pk':p.inf_sys}
	return HttpResponseRedirect(path)

#отображение информационной системы
def index(request, pk):
	try:
		data = {"ip": get_client_ip(request) ,'namehost':get_client_name(request), 'remotehost':get_host(request), 
			'devices': Device.objects.filter(inf_sys=pk), 'pk':Inf_sys.objects.filter(name=pk).last(), 'report':Report_scan.objects.filter(inf_sys=pk),
				'report1':Report_compare.objects.filter(inf_sys=pk).last(),
				 'state_task':State_of_task.objects.all().last()}
	except:
		data = {"ip": get_client_ip(request) ,'namehost':get_client_name(request), 'remotehost':get_host(request), 
		'devices': Device.objects.filter(inf_sys=pk), 'pk':Inf_sys.objects.filter(name=pk).last(), 'report':Report_scan.objects.filter(inf_sys=pk),
			'report1':Report_compare.objects.filter(inf_sys=pk).last()}
	return render(request, 'mainApp/wrapper.html',context=data)

#главная страницы
def index_home(request):
	data = { 'inf_syss': Inf_sys.objects.all()}
	return render(request, 'mainApp/wrapper_home.html',context=data)

#страница устройства
def device_detail(request,pk):
	ideales = Hash_service_conf.objects.filter(device_of_hash=Device.objects.filter(id=pk).last())
	try:
		report_scan = Report_scan_device.objects.filter(device=Device.objects.filter(id=pk).last())[0]
		report_compare = Report_compare_device.objects.filter(device=Device.objects.filter(id=pk).last())[0]
		state_task = State_of_task.objects.all().last()
	except:
		report_compare = ''
		report_scan = ''
		state_task = ''
	openports = Reference_port.objects.filter(device_of_port=pk)
	return render(request, 'mainApp/post.html', {'device':Device.objects.filter(id=pk).last(),
		 'services':Hash_service_conf.objects.filter(device_of_hash=Device.objects.filter(id=pk).last())
		 		,'report_scan':report_scan,
		 		'report_compare':report_compare,
		 		'win_progrmas':Win_program.objects.filter(device=Device.objects.filter(id=pk).last()),
		 		'lin_programs':Service.objects.filter(device=Device.objects.filter(id=pk).last()),
		 		'state':state_task,
		 		'openports':openports})
 

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
    
def get_host(request): 
    return request.META.get('HTTP_HOST')

def get_client_name(request): 
    return request.META.get('REMOTE_HOST')


#сравнение текущего состояния с эталонным
def compare_ssh(request,pk): 
	job = tasks.compare.delay(pk)
	return HttpResponseRedirect('/inf_sys/'+str(pk)+'/')

#создание эталона
def get_ssh1(request,pk): 
	job = tasks.scan.delay(pk)
	return HttpResponseRedirect('/inf_sys/'+str(pk)+'/')

#создание эталона для устройства
def get_ssh_device(request,pk): 
	job = tasks.scan_device.delay(pk)
	return HttpResponseRedirect('/'+str(pk)+'/')

#сравнение текущего состояния с эталонным (для устройства)
def ssh_compare_device(request,pk): 
	job = tasks.compare_device.delay(pk)
	return HttpResponseRedirect('/'+str(pk)+'/')

#функция сканирования для windows
def ssh_scan_windows(client,document,device, inf_system):
	document.add_heading("Programs", 0)
	InstLocation = "" # рабочий каталог программы
	cmd_names = 'wmic Product where InstallLocation!=Null get Name, InstallLocation, version /FORMAT:LIST' # получаем именя всех программ
	if device.smb == True:
		not_sorted_names = client.run_executable(executable, arguments="/c "+cmd_names)[0].decode('cp866').split('\n')
	else:	
		stdin, stdout, stderr = client.exec_command(cmd_names)
		not_sorted_names = stdout.read().decode('cp866').split('\n') #считываем имена
	#супер классная сортировка
	name_path_var = []
	names = []
	i = 0 # счетчик строк (их должно быть три: имя, паф, версия)
	for line in not_sorted_names:
		if len(line.rstrip())>1:
			name_path_var.append(line.rstrip().split('=')[1])
			i = i+1
		else:
			i = 0
			if len(name_path_var)>1:
				names.append(name_path_var)
				name_path_var = []
	names = list(filter(None, names)) # убираем пустые строки


	for name in names:
		vers = name[2]
		new_inst_loc = name[0]
		print(name[1])
		#добавялем новые программы
		if not Win_program.objects.filter(name=name[1].rstrip(), device=device).exists():
			win_prog = Win_program(name=name[1].rstrip(),
					device = device,
					path= new_inst_loc,
					version= vers )
			win_prog.save()

		InstLocation = new_inst_loc #считываем местоположение файлов
		document.add_paragraph(name[1]) 

		#находим все конф файлыфайлы
		if device.smb == True:
			files = client.run_executable(executable, arguments="/c "+'dir /s /b /n "'+InstLocation.rstrip()+'*.bat" "'+InstLocation.rstrip()+'"*.exe" "')[0].decode('cp866').split('\n')	
		else:	
			stdin, stdout, stderr = client.exec_command('dir /s /b /n "'+InstLocation.rstrip()+'*.bat" "'+InstLocation.rstrip()+'"*.exe" "')
			files = stdout.read().decode('cp866').split('\n') #считываем
		files = list(filter(None, files)) # убираем пустые строки
		for file in files:
			print(file)
			cmd_hash = 'Certutil -hashfile "'+file.rstrip()+'" SHA256' #считаем хэш файла 
			if device.smb == True:
				hash_file = client.run_executable(executable, arguments="/c "+cmd_hash)[0].decode('cp866').split('\n')[1]
			else:
				stdin_hash, stdout_hash, stderr_hash = client.exec_command(cmd_hash)
				hash_file = stdout_hash.read().decode('cp866').split('\n')[1] #считываем хэш файла 
			text = '-'
			#если не .exe, то записываем текст файла  БД
			if file.lower().find(".exe") == -1:
				if device.smb == True:
					text = client.run_executable(executable, arguments="/c "+"powershell cat '"+file.rstrip()+"'")[0].decode('cp866')
				else:
					stdin, stdout, stderr = client.exec_command("powershell cat '"+file.rstrip()+"'")
					text = stdout.read().decode('cp866')
				#text = re.sub(r'[^!?,.\w\n ]+', '', text)
				print(text)
				#записываем хэщ
			if not Hash_service_conf.objects.filter(name=name[1], file=file.rstrip(), device_of_hash=device, inf_sys=inf_system ).exists():
				hc = Hash_service_conf(name=name[1],
					name_windows=Win_program.objects.filter(name=name[1].rstrip())[0],  
					file= file.rstrip(),
					hash_conf=hash_file.rstrip(),
					device_of_hash=device,
					inf_sys=inf_system,
					text_conf=text.rstrip(),
					changed_text_conf=''
					)
				hc.save()
		

#функция сравнения для windows
def ssh_compare_windows(client,document,device, inf_system, now):
	document.add_heading("Changes in programs", 0)
	InstLocation = "" # рабочий каталог программы
	cmd_names = 'wmic Product where InstallLocation!=Null get Name, InstallLocation, version /FORMAT:LIST' # получаем именя всех программ
	if device.smb == True:
		not_sorted_names = client.run_executable(executable, arguments="/c "+cmd_names)[0].decode('cp866').split('\n')
	else:
		stdin, stdout, stderr = client.exec_command(cmd_names)
		not_sorted_names = stdout.read().decode('cp866').split('\n') #считываем имена
	name_path_var = []
	names = []
	i = 0 # счетчик строк (их должно быть три: имя, паф, версия)
	for line in not_sorted_names:
		if len(line.rstrip())>1:
			name_path_var.append(line.rstrip().split('=')[1])
			i = i+1
		else:
			i = 0
			if len(name_path_var)>1:
				names.append(name_path_var)
				name_path_var = []
	names = list(filter(None, names)) # убираем пустые строки

	for name in names:
		print(name[1])
		vers = name[2]
		new_inst_loc = name[0]

		if not Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system).extra(where=['name=%s'], params=[name[1].rstrip()]).exists():
			print("new programm")
			document.add_paragraph('New program: '+name[1]+':'+vers)

		InstLocation = new_inst_loc #считываем местоположение файлов
		
		if device.smb == True:
			files = client.run_executable(executable, arguments="/c "+'dir /s /b /n "'+InstLocation.rstrip()+'*.bat" "'+InstLocation.rstrip()+'*".exe" "')[0].decode('cp866').split('\n') 
		else:
			stdin, stdout, stderr = client.exec_command('dir /s /b /n "'+InstLocation.rstrip()+'*.bat" "'+InstLocation.rstrip()+'*".exe" "') # ищем список файлов для хэширования
			files = stdout.read().decode('cp866').split('\n') #считываем
		files = list(filter(None, files)) # убираем пустые строки
		document.add_paragraph(name[1]+":")

		
		for file in files:
			#определение новых файлов
			if not Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system).extra(where=['file=%s' , 'name=%s'], params=[file.rstrip(),name[1].rstrip()]).exists():
				text = '-'
				document.add_paragraph('New file: '+file.rstrip())
				cmd_hash = 'Certutil -hashfile "'+file.rstrip()+'" SHA256' #считаем хэш файла 
				if device.smb == True:
					hash_file = client.run_executable(executable, arguments="/c "+cmd_hash)[0].decode('cp866').split('\n')[1]
				else:
					stdin_hash_new, stdout_hash_new, stderr_hash_new = client.exec_command(cmd_hash)
					hash_file = stdout_hash_new.read().decode('cp866').split('\n')[1] #считываем хэш файла
				if file.lower().find('.exe') == -1:
					if device.smb == True:
						text = client.run_executable(executable, arguments="/c "+"powershell cat '"+file.rstrip()+"'")[0].decode('cp866')
					else:
						stdin, stdout, stderr = client.exec_command("powershell cat '"+file.rstrip()+"'")
						text = stdout.read().decode('cp866')
					#text = re.sub(r'[^!?,.\w\n ]+', '', text)
				try:
					hc = Hash_service_conf(name=name[1].rstrip(),
						name_windows=Win_program.objects.filter(name=name[1].rstrip())[0],  
						file= file.rstrip(),
						hash_conf=hash_file.rstrip(),
						device_of_hash=device,
						inf_sys=inf_system,
						text_conf=text,
						changed_text_conf='',
						new=True)
					hc.save()	
				except:
					print("fail:"+file)
					continue
			cmd_hash = 'Certutil -hashfile "'+file.rstrip()+'" SHA256' #считаем хэш файла 
			if device.smb == True:
				hash_file = client.run_executable(executable, arguments="/c "+cmd_hash)[0].decode('cp866').split('\n')[1]
			else:
				stdin_hash, stdout_hash, stderr_hash = client.exec_command(cmd_hash)
				hash_file = stdout_hash.read().decode('cp866').split('\n')[1] #считываем хэш файла
			if len(hash_file) < 1:
				continue
			try:
				#сравнение
				if Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system, name=name[1].rstrip()).extra(where=['file=%s'], params=[file.rstrip()])[0].hash_conf != hash_file.rstrip(): 
					document.add_paragraph('Hash of file: '+file+ ' is changed')
					ChangedFile = Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system, name=name[1].rstrip()).extra(where=['file=%s'], params=[file.rstrip()])[0]
					ChangedFile.changed = True
					#compare text confs
					if device.smb == True:
						text_new = client.run_executable(executable, arguments="/c "+"powershell cat '"+file.rstrip()+"'")[0].decode('cp866').splitlines(keepends=True)
					else:
						stdin, stdout, stderr = client.exec_command("powershell cat '"+file.rstrip()+"'")
						text_new = stdout.read().decode('cp866').splitlines(keepends=True)
					text_old = ChangedFile.text_conf.splitlines(keepends=True)
					d = Differ()
					result_text = list(d.compare(text_old, text_new))
					res = ''
					for line in result_text:
						if (line[0] == '+' or line[0] == '-') and (str(ChangedFile.changed_text_conf).find(line) == -1) :
							res = res + line
					ChangedFile.changed_text_conf = str(ChangedFile.changed_text_conf) + res
					ChangedFile.save()
			except:
				pass
			try:
				fileCheck = Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system).extra(where=['file=%s', 'name=%s'], params=[file.rstrip(), name[1].rstrip()])[0]
				fileCheck.timestamp = str(now)
				fileCheck.save()
			except:
				pass

		old_files = Hash_service_conf.objects.filter(Q(device_of_hash=device), Q(inf_sys=inf_system), ~Q(timestamp=str(now)) ).extra(where=['name=%s'], params=[name[1].rstrip()])
		for file in old_files:
			document.add_paragraph('Deleted files '+file.name.rstrip()+':')
			document.add_paragraph(file.file.rstrip())
			file.deleted = True
			file.save()

		
#функция сканирования для linux
def ssh_scan_linux(client,document, device, inf_system):

	document.add_heading("Services", 0)
	cmd_get_progs = "dpkg -l | grep ^ii | awk '{ print $2}'" # получаем именя всех программ
	stdin, stdout, stderr = client.exec_command(cmd_get_progs)
	progs = stdout.read().decode('cp866').split('\n') #считываем имена
	progs = list(filter(None, progs))
	#progs = ['apache2','nginx-common']
	for prog in progs:
		#print(prog)
		stdin_a, stdout_a, stderr_a = client.exec_command("dpkg -L "+ prog+ ' | grep -e ".*\\.cnf$" -e ".*\\.conf$"')
		files = stdout_a.read().decode('cp866').split('\n')
		files = list(filter(None, files))
		if not Service.objects.filter(name=prog.rstrip(), device=device).exists() :
			if len(files) > 0:
				stdin, stdout, stderr = client.exec_command("dpkg -s "+prog.rstrip()+ " | grep Version: | awk '{print $2}' ")
				vers = stdout.read().decode('cp866')
				lin_prog = Service(name=prog.rstrip(),
							inf_sys=inf_system,
							device = device,
							path= '-',
							version=vers)
				lin_prog.save()
			else:
				continue
			

		document.add_paragraph(prog) 

		print(files)
		for file in files:
			cmd_hash = 'sha256sum '+file.rstrip()+" | awk '{ print $1 }'" #считаем хэш файла 
			stdin_hash, stdout_hash, stderr_hash = client.exec_command(cmd_hash)
			hash_file = stdout_hash.read().decode('cp866').split('\n')[0] #считываем хэш файла 


			stdin, stdout, stderr = client.exec_command('cat "'+file+'"')
			text = stdout.read().decode('cp866')
			if not Hash_service_conf.objects.filter(name=prog.rstrip(), file=file.rstrip(), device_of_hash=device, inf_sys=inf_system ).exists():
				hc = Hash_service_conf(name=prog.rstrip(),
					name_linux=Service.objects.filter(name=prog.rstrip())[0],  
					file= file.rstrip(),
					hash_conf=hash_file.rstrip(),
					device_of_hash=device,
					inf_sys=inf_system,
					text_conf=text,
					changed_text_conf='-')
				hc.save()



#функция сравнения для linux
def ssh_compare_linux(client,document,device, inf_system,now):
	#now = datetime.datetime.now()
	document.add_heading("Changes in programs", 0)
	cmd_names = "dpkg -l | grep ^ii | awk '{ print $2}'"  # получаем именя всех программ
	stdin, stdout, stderr = client.exec_command(cmd_names)
	names = stdout.read().decode('cp866').split('\n') #считываем имена
	names = list(filter(None, names))
	#names = ['apache2','nginx-common']
	for name in names:
		print(name)


		stdin_a, stdout_a, stderr_a = client.exec_command("dpkg -L "+ name.rstrip()+ ' | grep -e ".*\\.cnf$" -e ".*\\.conf$"')# ищем список файлов для хэширования
		document.add_paragraph(name+":")
		files = stdout_a.read().decode('cp866').split('\n') #считываем
		files = list(filter(None, files))
		if not Service.objects.filter(device=device, inf_sys=inf_system, name=name.rstrip()).exists():
			if len(files) > 0:
				stdin, stdout, stderr = client.exec_command("dpkg -s "+prog.rstrip()+ " | grep Version: | awk '{print $2}' ")
				vers = stdout.read().decode('cp866')
				document.add_paragraph('New program: '+name.rstrip()+':'+vers.rstrip())
			else:
				continue
			
		for file in files:
			print(file)
			if not Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system, name=name.rstrip()).extra(where=['file=%s'], params=[file]).exists():
				document.add_paragraph('New file: '+file)
				cmd_hash = 'sha256sum '+file.rstrip()+" | awk '{ print $1 }'" #считаем хэш файла 
				stdin_hash_new, stdout_hash_new, stderr_hash_new = client.exec_command(cmd_hash)
				hash_file = stdout_hash_new.read().decode('cp866').split('\n')[0] #считываем хэш файла
				stdin, stdout, stderr = client.exec_command('cat "'+file+'"')
				text = stdout.read().decode('cp866')
				hc = Hash_service_conf(name=name.rstrip(),
					name_linux=Service.objects.filter(name=name.rstrip())[0],  
					file= file.rstrip(),
					hash_conf=hash_file.rstrip(),
					device_of_hash=device,
					inf_sys=inf_system,
					text_conf=text,
					changed_text_conf='',
					new=True)
				hc.save()

			cmd_hash = 'sha256sum '+file.rstrip()+" | awk '{ print $1 }'" #считаем хэш файла 
			stdin_hash, stdout_hash, stderr_hash = client.exec_command(cmd_hash)
			hash_file = stdout_hash.read().decode('cp866').split('\n')[0] #считываем хэш файла
			if len(hash_file) < 1:
				continue
			try:
				if Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system, name=name.rstrip()).extra(where=['file=%s'], params=[file])[0].hash_conf != hash_file.rstrip(): 
					document.add_paragraph('Hash of file: '+file+ ' is changed')
					#compare hash files
					ChangedFile = Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system, name=name.rstrip()).extra(where=['file=%s'], params=[file])[0]
					ChangedFile.changed = True
					#compare text confs
					stdin_text, stdout_text, stderr_text = client.exec_command('cat "'+file+'"')
					text_new = stdout_text.read().decode('cp866').splitlines(keepends=True)
					text_old = ChangedFile.text_conf.splitlines(keepends=True)
					d = Differ()
					result_text = list(d.compare(text_old, text_new))
					res = ''
					for line in result_text:
						if (line[0] == '+' or line[0] == '-') and (str(ChangedFile.changed_text_conf).find(line) == -1) :
							res = res + line	
					ChangedFile.changed_text_conf = str(ChangedFile.changed_text_conf) + res
					ChangedFile.save()
			except:
				pass

			try:
				fileCheck = Hash_service_conf.objects.filter(device_of_hash=device, inf_sys=inf_system, name=name.rstrip()).extra(where=['file=%s'], params=[file.rstrip()])[0]
				fileCheck.timestamp = str(now)
				fileCheck.save()
			except:
				pass



		old_files = Hash_service_conf.objects.filter(Q(device_of_hash=device), Q(inf_sys=inf_system), Q(name=name.rstrip()), ~Q(timestamp=str(now)) )
		for file in old_files:
			document.add_paragraph('Deleted files '+file.name.rstrip()+':')
			document.add_paragraph(file.file.rstrip())
			file.deleted = True
			file.save()


