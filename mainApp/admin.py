from django.contrib import admin
from mainApp.models import Device
from mainApp.models import Service
from mainApp.models import Reference_port
from mainApp.models import Hash_service_conf
from mainApp.models import Inf_sys
from mainApp.models import Report_scan
from mainApp.models import Report_compare
from mainApp.models import Win_program
from mainApp.models import State_of_task
from mainApp.models import Report_scan_device
from mainApp.models import Report_compare_device

admin.site.register(Device)
admin.site.register(Service)
admin.site.register(Reference_port)
admin.site.register(Hash_service_conf)
admin.site.register(Inf_sys)
admin.site.register(Report_scan)
admin.site.register(Report_compare)
admin.site.register(Win_program)
admin.site.register(State_of_task)
admin.site.register(Report_scan_device)
admin.site.register(Report_compare_device)


